﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CsvHelper;
using System.IO;

namespace WSN.Report
{
    public static class PAMReport
    {
        public static void Generate()
        {
            var writer = new CsvWriter(new StreamWriter(string.Format("PAM_Report_{0}", DateTime.Now.ToString("HH:mm:ss"))));
            writer.WriteHeader<Log>();

            foreach (var record in Logger.Instance.Logs)
            {
                writer.WriteRecord(record);
                writer.NextRecord();
            }
        }
    }
}
