﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSN.Devices.Sensor
{
    public class Sensor : ISensor, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private int _value;
        [JsonProperty("value")]
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyChanged("OutputValue");
            }
        }

        private string _id;
        [JsonProperty("id")]
        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
                OnPropertyChanged("OutputID");
            }
        }

        public string OutputID
        {
            get
            {
                return ID.ToString();
            }
        }

        public virtual string OutputValue
        {
            get
            {
                return Value.ToString();
            }
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
