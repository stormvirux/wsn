﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WSN.IoT;
using WSN.Devices.Sensor;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.IO;

namespace WSN.Devices
{
    public class Coordinator
    {
        public SerialPort SP { get; private set; }
        public ISensor Temperature { get; set; }
        public Accel Accel { get; set;}
        public InfoImage Battery { get; set; }
        public InfoImage SignalStrength { get; set; }

        List<string> AccelTemp = new List<string>() { "0", "0", "0" };

        XmlSerializer SerializerAccel = new XmlSerializer(typeof(Sensor.Sensor));
        private string DataFName = Path.Combine(System.Environment.CurrentDirectory, "data", System.DateTime.Now.ToString("HH-mm-ss") + ".xml");

        /// <summary>
        /// Initialises the serial port named 'port' and initialises all fields
        /// </summary>
        /// <param name="port">string of port name e.g. COM4</param>
        public Coordinator(string port)
        {
            SP = new SerialPort();
            SP.PortName = port;
            SP.Parity = Parity.Even;
            SP.BaudRate = 9600;

            SP.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

            try
            {
                SP.Open();
            }
            catch (Exception ex)
            {
                Logger.Instance.Log("Coord", ex.Message);
            }

            Battery = new InfoImage("_battery");
            SignalStrength = new InfoImage("_signalStrength");
            Temperature = new Temperature();
            Accel = new Accel();
        }

        /// <summary>
        /// Returns the Json string sent by the Coordinator
        /// </summary>
        /// <returns>Json of end devices sensor data</returns>
        public void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            string s = string.Empty;

            if (SP.IsOpen)
            {
                s = ParseSPDataToJson(SP.ReadLine());
            }
            //return s;
        }

        /// <summary>
        /// Sends data to the Coordinator
        /// </summary>
        /// <param name="s">Json/Zigbee command to send to Coordinator</param>
        /// <returns></returns>
        public bool SendData(string s)
        {
            bool b = false;
            if (SP.IsOpen)
            {
                SP.WriteLine(s);
                b = true;
            }
            return b;
        }

        /// <summary>
        /// Converts the Zigbee/Json data returned by the Coordinator into proper Json
        /// </summary>
        /// <param name="data">Zigbee/Json data</param>
        /// <returns>parsed Json data</returns>
        private string ParseSPDataToJson(string data)
        {
            if (SP.IsOpen)
            {
                //Logger.Instance.Log("API", data);
                if (!string.IsNullOrEmpty(data) && data.Contains("{name:"))
                {
                    String jsonString = data
                        .Replace(";", ",")
                        .Replace("~recv~:", String.Empty)
                        .Replace("name:", "\"name\":\"")
                        .Replace(",channel:", "\",\"channel\":\"")
                        .Replace(",light:", "\",\"light\":\"")
                        .Replace(",volt:", "\",\"volt\":\"")
                        .Replace(",temp:", "\",\"temp\":\"")
                        .Replace(",\"accel\":", "\",\"accel\":")
                        .Replace("X:", String.Empty)
                        .Replace("Y:", String.Empty)
                        .Replace("Z:", String.Empty)
                        .ToString();

                    try
                    {
                        if (!string.IsNullOrEmpty(jsonString))
                        {
                            SocketJSON json = JsonConvert.DeserializeObject<SocketJSON>(jsonString);
                            Temperature.ID = json.Name;
                            Temperature.Value = json.Temp;
                            
                            Accel.ID = json.Name;
                            
                            for (int a = 0; a < json.Accel.Count; a++)
                            {
                                sbyte s = Accel.ConvertIntToSByte(json.Accel[a]);
                                AccelTemp[a] = Accel.ConvertSByteToG(s).ToString();
                            }
                            
                            //var activity = CheckActivity(AccelTemp);
                            //AccelTemp[0] = activity;
                            Accel.Value = AccelTemp;

                            

                            var volt = (json.Voltage / 1000);
                            var voltCheck = volt> 3 ? 3 : volt; // Assume we are on battery to max it at 3V
                            var convertToPercentage = (voltCheck / 3) * 100; //2.8V Battery, 3.3V USB
                            Battery.Value = convertToPercentage;

                            // Save accelerometer value to XML. 
                            //SaveAccelXML();
                        }
                    }
                    catch (JsonException ex)
                    {
                        Logger.Instance.Log("API", ex.Message);
                    }

                    return jsonString;
                }
                else
                {
                    Logger.Instance.Log("API", "Unable to read Serial Port");
                    Logger.Instance.Log("API", "Fixing Serial Port...");
                    SP.DiscardInBuffer();
                    SP.Close();
                    SP.Open();
                    Logger.Instance.Log("API", "Fixing Serial Port...done");
                }   
            }
            return string.Empty;
        }

        /// <summary>
        /// Serialises and saves the Accel object to XML. 
        /// </summary>
        public void SaveAccelXML()
        {
            
            TextWriter WriteFileStream = new StreamWriter(DataFName);
            SerializerAccel.Serialize(WriteFileStream, Accel);

            WriteFileStream.Close();
        }

        /// <summary>
        /// Load the Accel object from XML. 
        /// </summary>
        public void LoadAccelXML()
        {
            FileStream ReadFileStream = new FileStream(DataFName, FileMode.Open, FileAccess.Read, FileShare.Read);

            ReadFileStream.Close();
        }

        public string CheckActivity(List<string> next)
        {
            if (Math.Abs(Convert.ToDouble(next[0])) > 1.5 || Math.Abs(Convert.ToDouble(next[1])) > 1.5 || Math.Abs(Convert.ToDouble(next[2])) > 1.5)
                return "Running";
            return string.Empty;
        }

        /// <summary>
        /// Checks current accelerometer data against thresholds and determine whether running is taking place. Crude. 
        /// Invoked by timer from main routine. 
        /// </summary>
        /// <param name="stateInfo"></param>
        public void ActivityChecker(Object stateInfo)
        {
            if (Math.Abs(Convert.ToDouble(AccelTemp[0])) > 1.0 || Math.Abs(Convert.ToDouble(AccelTemp[1])) > 1.0 || Math.Abs(Convert.ToDouble(AccelTemp[2])) > 1.0)
                Logger.Instance.Log("Coord", string.Format("Activity {0} = Running", Accel.ID));
            else if (Math.Abs(Convert.ToDouble(AccelTemp[0])) > 0.8 || Math.Abs(Convert.ToDouble(AccelTemp[1])) > 0.5 || Math.Abs(Convert.ToDouble(AccelTemp[2])) > 1.0)
                Logger.Instance.Log("Coord", string.Format("Activity {0} = Walking", Accel.ID));
            else
                Logger.Instance.Log("Coord", string.Format("Activity {0} = Stationary", Accel.ID));
        }

        public enum LEDColor
        {
            Red,
            Green
        }

        /// <summary>
        /// LED structure where led is the colour of the LED you want to set
        /// and state is the on/off state of that led
        /// </summary>
        public struct LED
        {
            public LEDColor led;
            public bool state;
        }

        /// <summary>
        /// Sets a single led to the 'state'
        /// </summary>
        /// <param name="led">The led structure</param>
        public void SetLED(LED led)
        {
            //string device = string.Empty;
            string action = led.led == LEDColor.Red ? "led1" : "led2";
            string actionArgs = led.state == true ? "1" : "0";
            string sendLed = led.led == LEDColor.Red ? "send1" : "send2";
            string sendArgs = led.state == true ? "1" : "0";
            //string value = "{" + string.Format("{0}:{1}; {2}:{3}", action, actionArgs, sendLed, sendArgs) + "}";
            string value = "{" + string.Format("{0}:{1}", action, actionArgs) + "}";

            // so like
            // {action="led", led1:led.red led2:led.green send1:true send2:false}

            // {name:A8, command:action, dev:led }

            String name = "A8";
            String cmd = "action";

            //SendData(device, cmd, args);

            // format ?? {name:A8; command:action; dev:led; value:[led1:1;led2:1;send1:1;send2:1] }
            //SendDataToSerialPort("{name:A8; command:action; dev:led; conf:[led1:1;led2:1;send1:1;send2:1] }");
            //SendDataToSerialPort(String.Format("{ name:{0} command:{1} dev:{2} value:{3} }", name, cmd, device, value));
            //_coordinator.WriteLine("{name:A8; command:action; dev:led; value:[led1:1;led2:1] }");
            var data = "{" + string.Format("name:{0}; command:{1}; dev:led; value:{2}", name, cmd, value) + "}";
            SendData(data);
        }

        /// <summary>
        /// Sets both LEDs on/off according to settings
        /// </summary>
        /// <param name="red">Red LED Settings</param>
        /// <param name="green">Green LED Settings</param>
        public void SetLEDs(LED red, LED green)
        {
            SetLED(red);
            SetLED(green);
        }
    }
}
