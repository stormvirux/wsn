﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using WSN.Devices.Sensor;
using System.Net.Http;
using System.IO.Ports;

namespace WSN.IoT
{
    /// <summary>
    /// This class is now depreciated
    /// </summary>
    [Obsolete("Depreciated. Use WSN.Devices.Coordinator", true)]
    public class API
    {
        public string IP = "localhost";
        public string Port = "3000";
        private SerialPort _coordinator = new SerialPort();

        public void GetTemperature(ref Temperature t)
        {
            string device = string.Empty;
            string req = "temp";
            string format = "json";
            var data = GetData(device, req, format);

            if (data != string.Empty)
                t = JsonConvert.DeserializeObject<Temperature>(data);
        }

        public async Task<Temperature> GetTemperatureAsync()
        {
            var t = new Temperature();
            string device = string.Empty;
            string req = "temp";
            string format = "json";

            var data = await GetDataAsync(device, req, format);

            if (data != string.Empty)
                t = JsonConvert.DeserializeObject<Temperature>(data);

            return t;
        }

        public void GetAccel(ref Accel a)
        {
            string device = string.Empty;
            string req = "accel"; // Addon
            string format = "json";
            var data = GetData(device, req, format);

            if (data != string.Empty)
                a = JsonConvert.DeserializeObject<Accel>(data);
        }

        public async Task<Accel> GetAccelAsync()
        {
            var a = new Accel();
            string device = string.Empty;
            string req = "accel";
            string format = "json";
            var data = await GetDataAsync(device, req, format);

            if (data != string.Empty)
                a = JsonConvert.DeserializeObject<Accel>(data);

            return a;
        }

        public void GetSignalStrength(ref InfoImage sig)
        {
            string device = string.Empty;
            string req = "lqi";
            string format = "json";
            var data = GetData(device, req, format);

            if (data != string.Empty)
                sig.Value = int.Parse(data);
        }

        public async Task<InfoImage> GetSignalStrengthAsync()
        {
            var sig = new InfoImage("signalStrength");
            string device = string.Empty;
            string req = "lqi";
            string format = "json";
            var data = await GetDataAsync(device, req, format);

            if (data != string.Empty)
                sig.Value = int.Parse(data);

            return sig;
        }

        public void GetBattery(ref InfoImage bat)
        {
            string device = string.Empty;
            string req = "voltage"; //?
            string format = "json";
            var data = GetData(device, req, format);

            if (data != string.Empty)
                bat.Value = int.Parse(data);
        }

        public async Task<InfoImage> GetBatteryAsync()
        {
            var bat = new InfoImage("battery");
            string device = string.Empty;
            string req = "voltage"; //?
            string format = "json";
            var data = await GetDataAsync(device, req, format);

            if (data != string.Empty)
                bat.Value = int.Parse(data);

            return bat;
        }

        private string GetData(string device, string req, string format = "")
        {
            if (IsConnected(IP, Port))
            {
                string url = string.Format("http://{0}/control/feed.php", IP);
                string inputs = string.Format("?device={0}&request={1}&format{2}", device, req, format);

                var webReq = (HttpWebRequest)WebRequest.Create(url + inputs);

                webReq.Timeout = 2500;
                webReq.Method = "GET";

                var webResp = (HttpWebResponse)webReq.GetResponse();

                var response = new StreamReader(webResp.GetResponseStream());

                return response.ReadToEnd();
            }
            else
                return string.Empty;
        }

        private async Task<string> GetDataAsync(string url)
        {
            var client = new HttpClient();
            // 3 second timeout
            client.Timeout = new TimeSpan(0, 0, 3);
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url));
            var response = await client.SendAsync(request);
            return response.Content.ReadAsStringAsync().Result;
        }

        private async Task<string> GetDataAsync(string device, string reqOrCmd, string args = "")
        {
            var client = new HttpClient();
            // 3 second timeout
            client.Timeout = new TimeSpan(0, 0, 3);
            var request = new HttpRequestMessage(HttpMethod.Get, new Uri(device + reqOrCmd + args));
            var response = await client.SendAsync(request);
            return response.Content.ReadAsStringAsync().Result;
        }

        public bool InitCoordinator(string m)
        {
            _coordinator.PortName = m;
            _coordinator.BaudRate = 9600;
            _coordinator.Parity = Parity.Even;
            _coordinator.Open();
            return _coordinator.IsOpen;
        }

        

        public string GetCurrentSerialPortStatus()
        {
            if (_coordinator.IsOpen)
                return _coordinator.ReadLine();
            else
                return string.Empty;
        }

        public void SendDataToSerialPort(String m)
        {
            if (_coordinator.IsOpen)
                _coordinator.WriteLine(m);
        }

        public void StopCoordinator()
        {
            if (_coordinator.IsOpen)
                _coordinator.Close();
        }

        /*
         * Command List: led.red, led.green, shutdown, network
         * Arguments: true, false, confirm_shutdown_arg, restart
         */
        private string SendData(string device, string cmd, string args = "")
        {
            if (IsConnected(IP, Port))
            {
                string url = string.Format("http://{0}/control/input.php", IP);
                string inputs = string.Format("?device={0}&command={1}&args{2}", device, cmd, args);

                var webReq = (HttpWebRequest)WebRequest.Create(url + inputs);

                webReq.Timeout = 2500;
                webReq.Method = "GET";

                var webResp = (HttpWebResponse)webReq.GetResponse();

                var response = new StreamReader(webResp.GetResponseStream());

                return response.ReadToEnd();
            }
            else
                return string.Empty;
        }

        public bool IsConnected(string ip, string port)
        {
            try
            {
                var webReq = (HttpWebRequest)WebRequest.Create(string.Format("http://{0}/", ip));
                webReq.Timeout = 5000;
                webReq.Method = "GET";

                var webResp = (HttpWebResponse)webReq.GetResponse();

                return webResp.StatusCode == HttpStatusCode.OK;
            }
            catch (WebException ex)
            {
                Logger.Instance.Log(this.GetType().ToString(), ex.Message);
                return false;
            }
        }
    }
}
