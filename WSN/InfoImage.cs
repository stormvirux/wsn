﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace WSN
{
    public class InfoImage
    {
        public event PropertyChangedEventHandler PropertyChanged;
 
        public void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public BitmapImage Image
        {
            get
            {
                return GetImageFromValue();
            }
        }

        private int _value;
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                OnPropertyChanged("Image");
            }
        }

        public InfoImage(string name)
        {
            Name = name;
        }

        private BitmapImage GetImageFromValue()
        {
            string val = string.Empty;
            if (Value >= 0 && Value < 25)
                val = "1";
            else if (Value > 25 && Value <= 50)
                val = "2";
            else if (Value > 50 && Value <= 75)
                val = "3";
            else if (Value > 75 && Value <= 100)
                val = "4";

            string pathToImg = string.Format("Content/Images/{0}_{1}.png", Name, val);
            var bmImg = new BitmapImage();
            bmImg.BeginInit();
            bmImg.UriSource = new Uri(pathToImg, UriKind.Relative);
            bmImg.EndInit();

            return bmImg;
        }
    }
}
