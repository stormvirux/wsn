﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSN
{
    public class Logger
    {
        public EventHandler OnItemAdd;
        private List<Log> _logs;
        public List<Log> Logs
        {
            get { return _logs; }
            set { _logs = value; }
        }

        private static object syncRoot = new Object();
        private static Logger instance;
        public static Logger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Logger();
                    }
                }
                return instance;
            }
        }

        public Logger()
        {
            _logs = new List<Log>();
        }

        private string LogFName = Path.Combine(System.Environment.CurrentDirectory, System.DateTime.Now.ToString("HH-mm-ss") + ".txt");
        public void DebugLog(string msg)
        {
            Debug.WriteLine(msg);
        }

        public void Log(string msgName, string msgDesc, string msgState = "Stationary")
        {
            using (var sw = File.AppendText(LogFName))
            {
                var l = new Log();
                l.EventName = msgName;
                l.EventDate = DateTime.Now.ToString("HH:mm:ss");
                l.EventDescription = msgDesc;
                l.SystemState = msgState;
                _logs.Add(l);

                OnItemAdd(this, null);

                sw.WriteLine(l.ToString());
                DebugLog(l.ToString());

            }
        }

        public void LogToFile(string msgName, string msgDesc)
        {
            using (var sw = File.AppendText(LogFName))
            {
                var l = new Log();
                l.EventName = msgName;
                l.EventDate = DateTime.Now.ToString("HH:mm:ss");
                l.EventDescription = msgDesc;
                l.SystemState = "Functional";

                sw.WriteLine(l.ToString());
                DebugLog(l.ToString());

            }
        }

        public List<Log> LogFileToList(string file)
        {
            var list = new List<Log>();

            using (StreamReader sr = new StreamReader(File.Open(file, FileMode.Open)))
            {
                while(!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var split = line.Split(',');
                    if (split.Length > 0)
                    {
                        list.Add(
                            new Log()
                            {
                                EventDate = split[0],
                                EventName = split[1],
                                EventDescription = split[2],
                                SystemState = split[3]
                            });
                    }
                }
            }

            return list;
        }
    }

    public class Log
    {
        public string EventName {get; set;}
        public string EventDate { get; set; }
        public string EventDescription { get; set; }
        public string SystemState { get; set; }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", EventDate, EventName, EventDescription, SystemState);
        }
    }
}
