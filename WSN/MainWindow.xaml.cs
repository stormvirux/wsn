﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WSN.Devices.Sensor;
using WSN.IoT;
using System.Threading;
using System.ComponentModel;
using MahApps.Metro.Controls;
using MahApps.Metro;
using System.IO.Ports;
using WSN.Devices;
using WSN.Report;

namespace WSN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private Thread _auto;

        public Coordinator Coordinator;

        public AutoResetEvent AutoEvent;
        public TimerCallback Tcb;
        public Timer StateTimer;

        public MainWindow()
        {
            InitializeComponent();
            InitFields();            
            SetDataContexts();
            SetupEvents();
        }

        private void InitFields()
        {
            Coordinator = new Coordinator("COM4");
        }

        private void SetDataContexts()
        {
            BatteryIcon.DataContext = Coordinator.Battery;
            SignalIcon.DataContext = Coordinator.SignalStrength;
            AccelTB.DataContext = Coordinator.Accel;
            TemperatureTB.DataContext = Coordinator.Temperature;
        }

        private void SetupEvents()
        {
            this.KeyDown += new System.Windows.Input.KeyEventHandler(onKeyPressed);
            Logger.Instance.OnItemAdd += MainWindow_OnItemAdd;

            

            if (Coordinator.SP.IsOpen)
            {
                _auto = new Thread(() => { UpdateData(); });
                _auto.Start();

                // Timer to execute method at timed intervals so that activity does not sporadically change after each data collection. 
                // An event to signal the timeout count threshold in the timer callback. 
                AutoEvent = new AutoResetEvent(false);
                Tcb = Coordinator.ActivityChecker;

                AutoEvent.WaitOne(5000, false);

                // Define timer to signal delegate to call ActivityChecker. 
                StateTimer = new Timer(Tcb, AutoEvent, 1000, 2500);
            }
            else
                Logger.Instance.Log("API", "Unable to Init Port");
        }

        private void MainWindow_OnItemAdd(object sender, EventArgs e)
        {            
            logDataGrid.Dispatcher.BeginInvoke((Action)(
                () => 
                    logDataGrid.Items.Add(Logger.Instance.Logs.Last())
                    ));

            logDataGrid.Dispatcher.BeginInvoke((Action)(
                () => 
                    logDataGrid.ScrollIntoView(logDataGrid.Items[logDataGrid.Items.Count-1])
                    ));
        }

        private void onKeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S)
                _auto.Start();
            if (e.Key == Key.E)
                _auto.Abort();
            if (e.Key == Key.F)
                HiddenStatusBar.Visibility = HiddenStatusBar.IsVisible ? Visibility.Hidden : Visibility.Visible;
            if (e.Key == Key.R)
                PAMReport.Generate();
        }

        public void UpdateData()
        {
            Logger.Instance.Log("GUI", "Loading Information...");
            while (true)
            {
                //string s = Coordinator.DataReceivedHandler();
                //Dispatcher.BeginInvoke(new Action(() => HiddenStatusBar.Text = s));

                Thread.Sleep(1000);
            }            
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            _auto.Abort();
            Coordinator.SP.Close();

            // Release timer on close. 
            StateTimer.Dispose();

            Application.Current.Shutdown();
        }
    }
}
