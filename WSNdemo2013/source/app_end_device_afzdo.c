/**
 * @ingroup apps
 * @{
 * @file comms_router_afzdo.c
 * @brief Resets Radio, configures this device to be a Zigbee Router, joins a network, then sends a message to the coordinator once per second.
 * Uses the AF/ZDO interface. Reads the ambient light sensor and supply voltage and sends these in the message.
 * Also sends a message upon a motion interrupt from the accelerometer.
 * @note This matches example_simple_application_coordinator.c
 * @note This example will not compile with the IAR Kickstart edition because CODE + CONST exceeds 4kB
 * @see http://processors.wiki.ti.com/index.php/Tutorial_on_the_Examples and http://e2e.ti.com/support/low_power_rf/default.aspx
 *
 * $Rev: 602 $
 * $Author: dsmith $
 * $Date: 2010-06-16 13:09:46 -0700 (Wed, 16 Jun 2010) $
 *
 * YOU ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY
 * OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY,
 * TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL TEXAS INSTRUMENTS
 * OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
 * BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
 * INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES,
 * LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
 * CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */
#include "../HAL/hal.h"
#include "../HAL/hal_vti_cma3000_D01_accelerometer.h"
#include "../ZNP/znp_interface.h"
#include "../ZNP/application_configuration.h"
#include "../ZNP/znp_interface_spi.h"
#include "../ZNP/af_zdo.h"
#include "znp_simple_app_utils.h" 
#include "Messages/infoMessage.h"
#include "../configuration/configure.h"
#include <string.h>  

unsigned int sequenceNumber = 0;  //an application-level sequence number to track acknowledgements from server
extern unsigned char znpBuf[100];
extern signed int znpResult;      // from znp_interface
unsigned char tmp[100];
/** function pointer (in hal file) for the function that gets called when the timer generates an int*/
extern int load;
/** This is the current state of the application. 
 * Gets changed by other states, or based on messages that arrive. */
unsigned int state = STATE_ZNP_STARTUP;
/** The main application state machine */
void stateMachine();
/** Various flags between states */
unsigned int stateFlags = 0;
#define STATE_FLAG_SEND_INFO_MESSAGE 0x01
/** Handles timer interrupt */
void handleTimer();
Config config;
/** This is the function pointer (in hal file) for the ISR called when the accelerometer generates an interrupt*/
extern void (*accelerometerIsr)(void);
/** Handles Accelerometer interrupt */
void handleAccelerometer();
void gpioInput();
/** Variable to track the cause of the Info Message; whether it be CAUSE_SCHEDULED or CAUSE_MOTION etc.*/
unsigned char infoMessageCause = CAUSE_STARTUP;
struct infoMessage im;
struct header hdr;

/**Comment these out to only send a message on motion or timer respectively */
//#define SEND_MESSAGE_ON_MOTION
//#define SEND_MESSAGE_ON_TIMER
//extern void (*timerIsr)(void);
extern void (*srdyIsr)(void);
extern void (*buttonIsr)(void);
extern void (*timerIsr)(void);
extern void (*gpioIsr)(void);
void handleUartrecept(void);
void handleButtonPress(void);

int main(void)
{
	halInit();
	setLed(1);
	delayMs(200);
	clearLeds();
	HAL_DISABLE_INTERRUPTS();
	buttonIsr = &handleButtonPress;
	accelerometerIsr = &handleAccelerometer;
	unsigned int vlo = calibrateVlo();
	timerIsr = &handleTimer;
#ifdef SEND_MESSAGE_ON_MOTION
	halSpiInitAccelerometer(); // note: this puts the SPI port in a non-ZNP configuration; must init it for ZNP afterwards
	writeAccelerometerRegister(ACCEL_CTRL, G_RANGE_2 | I2C_DIS | MODE_MD_10 | MDET_NO_EXIT);    // Configure Accelerometer
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);                                         // 11 bit-time delay required when using SPI
	readAccelerometerRegister(ACCEL_INT_STATUS);  // clear the interrupt
	accelerometerIsr = &handleAccelerometer;
	halEnableAccelerometerInterrupt(WAKEUP_AFTER_ACCELEROMETER);
#endif
	srdyIsr = &handleUartrecept;
	// create the infoMessage. Most of these fields are the same, so we can create most of the message ahead of time.
	hdr.sequence = 0;  // this will be incremented each message
	hdr.version = INFO_MESSAGE_VERSION;
	hdr.flags = INFO_MESSAGE_FLAGS_NONE;
	im.header = &hdr;  // Note, if you have multiple similar message types then you can use the same header for all
	im.deviceType = DEVICETYPE_SMITH_ELECTRONCS_ROUTER_DEMO;
	im.deviceSubType = DEVICESUBTYPE_SMITH_ELECTRONCS_ROUTER_DEMO;
	im.numParameters = 3;
	loadConfig(&config, 0);
	initTime(config.time);
	initAccel();
	initIo(&(config.io));
	servoInit();
	gpioIsr = &gpioInput;
	HAL_ENABLE_INTERRUPTS();
	// run the state machine
	stateMachine();
}

void stateMachine()
{
	while (1)
	{
		switch (state)
		{
		case STATE_IDLE:
		{
			// note: other flags (for different messages or events) can be added here
			if(SRDY_IS_LOW())
			{
				handleUartrecept();
				state = STATE_COMMAND;
			}
			// our events
			else if(interrLight( &(config.light)))
			{
				dataToJson(config, tmp);
				state = STATE_SEND_INFO_MESSAGE;
			}

			// the one below is causing multiple INTs from accelerometer triggering multiple transmissions
			//else if(interrAccel( &(config.accel)))
			//{
				//dataToJson(config, tmp);
				//state = STATE_SEND_INFO_MESSAGE;
			//}
			break;
		}
		case STATE_ZNP_STARTUP:
		{
			/* Start the network; if fails then wait a second and try again. */
			signed int startResult = startZnp(END_DEVICE);

			while (startResult != ZNP_SUCCESS)
			{
				startResult = startZnp(END_DEVICE);
			}
			memcpy(hdr.mac, getMacAddress(), 8);
			state = STATE_SEND_INFO_MESSAGE;
			break;
		}
		case STATE_SEND_INFO_MESSAGE:
		{
			setLed(0); // Clear this to enable LED Mode
			afSendData(DEFAULT_ENDPOINT, DEFAULT_ENDPOINT, 0, 0x0006, tmp, 84);
			printAfIncomingMsgHeader(znpBuf);
			clearLeds(); // Clear this to enable LED Mode
			if (znpResult != ZNP_SUCCESS)
			{
				state = STATE_ZNP_STARTUP;
			}
			else
			{
				printf(tmp);
				state = STATE_IDLE;
			}
			break;
		}
		case STATE_COMMAND:
		{
			command(&config, znpBuf+3+17, tmp);
			state = STATE_SEND_INFO_MESSAGE;
			break;
		}
		default: //should never happen
		{
			state = STATE_ZNP_STARTUP;
		}
		break;
		}
	}
}

/** Handles timer interrupt ~err~*/
void handleTimer()
{
	if (state == STATE_IDLE && config.time.enable == true)
	{
		dataToJson(config, tmp);
		state = STATE_SEND_INFO_MESSAGE;
	}
}

/** Handles Accelerometer interrupt */
void handleAccelerometer()
{
	takeValueAccel(&(config.accel));
}

void handleButtonPress(void)
{
	if (state == STATE_IDLE && config.button.inter == true)
	{
		dataToJson(config, tmp);
		state = STATE_SEND_INFO_MESSAGE;
	}
}

void handleUartrecept(void)
{
	spiPoll();
}

void gpioInput()
{
	if (state == STATE_IDLE)
	{
		dataToJson(config, tmp);
		state = STATE_SEND_INFO_MESSAGE;
	}
}
