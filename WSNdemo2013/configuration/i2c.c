#include "i2c.h"
#include "../HAL/hal_cc2530znp_target_board.h"
#include <string.h>
#include "../Common/printf.h"
#include "pin.h"

char *getI2CConfig(Io *io)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:i2c;conf:{mode:");
	strcat(tmp, p_int(io->i2c));
	strcat(tmp, ";addr:");
	strcat(tmp, p_int(io->i2cAdd));
	strcat(tmp, ";sda:");
	strcat(tmp, p_int(io->sda+3));
	strcat(tmp, ";scl:");
	strcat(tmp, p_int(io->scl+3));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setI2CConfig(Io *io, char *conf)
{
	printf("Change I2C config");
	if (findAttr(conf, "mode"))
		io->i2c = atoi(findAttr(conf, "mode"));
	if (findAttr(conf, "addr"))
		io->i2cAdd = atoi(findAttr(conf, "addr"));
	if (findAttr(conf, "scl"))
	{
		io->scl = atoi(findAttr(conf, "scl"))-3;
		io->pin[io->scl].direction = OUTPUT;
		io->pin[io->scl].mode = DIGITAL;
		io->pin[io->scl].value = 1;
		configPin(&(io->pin[io->scl]));
	}
	if (findAttr(conf, "sda"))
	{
		io->sda = atoi(findAttr(conf, "sda"))-3;
		io->pin[io->sda].direction = OUTPUT;
		io->pin[io->sda].mode = DIGITAL;
		io->pin[io->sda].value = 1;
		configPin(&(io->pin[io->sda]));
	}
	// SPI is already launched
	printf("I2C Configuration Enabled\r\n");
	return getI2CConfig(io);
}

char *getI2C(Io *io)
{
	char tmp[80] = "";
	unsigned char BytesTemp[2]={0x00,0x00};
	int temperature;

	if (io->i2c == NONE)
	{
		return tmp;
	}
	else if( io->i2c == COMPASS)
	{
		// HMC COMPASS
		I2C_start(io);
		I2C_txByte(io, 0x42);
		I2C_txByte(io,'A');
		I2C_stop(io);

		I2C_start(io);
		I2C_txByte(io, 0x43);
		tmp[0] = I2C_rxByte(io, ACK);
		tmp[1] = I2C_rxByte(io, NACK);
		I2C_stop(io);
		temperature = (tmp[0]<<8) | tmp[1];
		printf("Compass: %d ", temperature);
		strcpy(tmp, ";i2cb:");
		strcat(tmp, p_int(temperature));
	}
	else if(io->i2c == TEMP)
	{
		// TMP TEMPERATURE
		I2C_readBlock(io, io->i2cAdd, 2, BytesTemp);
		temperature = (BytesTemp[0]<<8) | BytesTemp[1];
		temperature >>= 4;
		strcpy(tmp, ";i2ct:");
		strcat(tmp, p_int(temperature));
	}
	else if (io->i2c == 0x01)
	{
		printf("Execution\r\n");
	}
	return tmp;
}

void I2C_init(void)							  // Unused
{
	PxOUT &= ~(SCL | SDA);                    // Output settings always LOW
	PxSEL &= ~(SCL | SDA);                    // Set GPIO functions
}

void I2C_delay(void)
{
	__delay_cycles(GPIODELAYCYCLES);          // Quick delay
}

void I2C_start(Io *io)
{
	setValuePin(io->pin[io->sda], 1);		  // SDA = 1
	I2CDELAY;                                 // Quick delay
	setValuePin(io->pin[io->scl], 1);		  // SCL = 1
	I2CDELAY;                                 // Quick delay
	setValuePin(io->pin[io->sda], 0);		  // SDA = 0
	I2CDELAY;                                 // Quick delay
	setValuePin(io->pin[io->scl], 0);		  // SCL = 0
	I2CDELAY;                                 // Quick delay
}

void I2C_stop(Io *io)
{
	setValuePin(io->pin[io->sda], 0);		  // SDA = 0
	I2CDELAY;                                 // Quick delay
	setValuePin(io->pin[io->scl], 1);         // SCL = 1
	I2CDELAY;                                 // Quick delay
	setValuePin(io->pin[io->sda], 1);         // SDA = 1
	I2CDELAY;                                 // Quick delay
}

unsigned char I2C_txByte(Io *io, unsigned char data)
{
	unsigned char bits, temp, ack;
	setValuePin(io->pin[io->scl], 0);         // SCL = 0
	temp = data;                              // Initialise temp variable
	bits = 0x08;                              // Load I2C bit counter
	while (bits != 0x00)                      // Loop until all bits are shifted
	{
		if (temp & BIT7)                      // Test data bit
			setValuePin(io->pin[io->sda], 1); // SDA = 1
		else
			setValuePin(io->pin[io->sda], 0); // SDA = 0
		I2CDELAY;                             // Quick delay
		setValuePin(io->pin[io->scl], 1);     // SCL = 1
		// while ((PxIN & SCL) == 0);         // Wait for any SCL clock stretching
		while (gettValuePin(io->pin[io->scl]) == 0);
		I2CDELAY;                             // Quick delay
		temp = (temp << 1);                   // Shift bits 1 place to the left
		setValuePin(io->pin[io->scl], 0);     // SCL = 0
		bits = (bits - 1);                    // Loop until 8 bits are sent
	}
	I2CDELAY;
	setValuePin(io->pin[io->sda], 1);         // SDA = 1
	setValuePin(io->pin[io->scl], 1);         // SCL = 1
	// while ((PxIN & SCL) == 0);             // Wait for any SCL clock stretching
	while (gettValuePin(io->pin[io->scl]) == 0);
	I2CDELAY;                                 // Quick delay
	ack = (PxIN & SDA);                       // Read ACK state from Slave
	setValuePin(io->pin[io->scl], 0);         // SCL = 0
	if (ack)                                  // Return ACK state to calling application
	{
		printf(" ERROR ACK \r\n");
		return (1);
	}
	else
		return (0);
}

unsigned char I2C_rxByte(Io *io, char ack)
{
	unsigned char bits, data = 0;

	setValuePin(io->pin[io->sda], 1);         // SDA = 1
	bits = 0x08;                              // Load I2C bit counter
	while (bits > 0)                          // Loop until all bits are read
	{
		setValuePin(io->pin[io->scl], 1);     // SCL = 1
		// while ((PxIN & SCL) == 0);         // Wait for any SCL clock stretching
		while (gettValuePin(io->pin[io->scl]) == 0);
		I2CDELAY;                             // Quick delay
		data = (data << 1);                   // Shift bits 1 place to the left
		if (PxIN & SDA)                       // Check digital input
		{data = (data + 1);}                  // If input is high, store a '1'
		setValuePin(io->pin[io->scl], 0);     // SCL = 0
		I2CDELAY;                             // Quick delay
		bits = (bits - 1);                    // Decrement I2C bit counter
	}
	if (ack)                                  // Need to send ACK to Slave?
		setValuePin(io->pin[io->sda], 0);     // Yes, so pull SDA low
	else
		setValuePin(io->pin[io->sda], 1);     // No, so keep SDA high
	setValuePin(io->pin[io->scl], 1);         // SCL = 1
	I2CDELAY;                                 // Equivalent to sending N(ACK)
	setValuePin(io->pin[io->scl], 0);         // SCL = 0
	setValuePin(io->pin[io->sda], 1);         // SDA = 1
	return (data);                            // Return 8-bit data byte
}

void I2C_writeBlock(Io *io, unsigned char SlaveAddress,
		unsigned int numBytes, unsigned char multi,
		void* TxData)
{
	unsigned int  i;
	unsigned char *temp;
	temp = (unsigned char *)TxData;           // Initialize array pointer
	I2C_start(io);                  		  // Send Start condition
	I2C_txByte(io, (SlaveAddress << 1) & ~BIT0); // [ADDR] + R/W bit = 0
	for (i = 0; i < numBytes; i++)
	{
		I2C_txByte(io, *(temp));       		  // Send data and ack
		temp++;                               // Increment pointer to next element
	}
	if (multi == 0)                           // Need STOP condition?
		I2C_stop(io);                         // Yes, send STOP condition
	I2CDELAY;                                 // Quick delay
}

void I2C_readBlock(Io *io, unsigned char SlaveAddress,
		unsigned int numBytes, void* RxData)
{
	unsigned int  i;
	unsigned char* temp;
	temp = (unsigned char *)RxData;           // Initialize array pointer
	I2C_start(io);              		      // Send Start condition
	I2C_txByte(io, (SlaveAddress << 1) | BIT0); // [ADDR] + R/W bit = 1
	//I2C_txByte(0x00); 					  // Change: temperature register
	for (i = 0; i < numBytes; i++)
	{
		if (i == (numBytes - 1))
			*(temp) = I2C_rxByte(io, NACK);   // Read last 8-bit data with no ACK
		else
			*(temp) = I2C_rxByte(io, ACK);    // Read 8-bit data & then send ACK
		temp++;                               // Increment pointer to next element
	}
	I2C_stop(io);  			                  // Send Stop condition
}
