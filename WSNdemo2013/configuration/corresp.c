#include "bool.h"
#include "corresp.h"
#include <string.h>
#include "../Common/printf.h"
#include "../Common/utilities.h"

void initCorresp(Corresp *corresp)
{
	int i;
	for( i = 0; i < NB_SENSORS; i++)
	{
		strcpy((corresp+i)->name,"-");
	}
}

int addCorresp(Corresp *corresp, char *name, unsigned int address)
{
	int i;
	if(strlen(name) <= 4)
	{
		for( i = 0; i < NB_SENSORS; i++)
		{
			if(strcmp(corresp[i].name,name) == 0)
				return 1;

			else if(strcmp(corresp[i].name, "-") == 0)
			{
				strcpy(corresp[i].name,name);
				printf("~new~:%s\r\n", corresp[i].name);
				corresp[i].adresse = address;
				return 1;
			}
		}
	}
	return -1;
}

void showCorresp(Corresp *corresp)
{
	int i;
	for( i = 0; i < 10; i++)
	{
		printf("|%s\t %X|\r\n", corresp[i].name, corresp[i].adresse);
	}
}

unsigned int findCorresp(Corresp *corresp, char *name)
{
	int i;
	for(i = 0; i < NB_SENSORS; i++)
	{
		if(strcmp((corresp+i)->name, name) == 0)
		{
			return (corresp+i)->adresse;
		}
	}
	return 0;
}

unsigned int deleteCorresp(Corresp *corresp, char *lastname)
{
	int i;
	for(i = 0; i < NB_SENSORS; i++)
	{
		if(strcmp((corresp+i)->name, lastname) == 0)
		{
			strcpy((corresp+i)->name, "-");
			return 0;
		}
	}
	return 0;
}
