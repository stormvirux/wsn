/**
 * \file 	servo.c
 * \brief 	low level functions for controlling a servo with MSP430F2740
 * \author 	Jan Szymanski
 * \date	September 2012
 *
* YOU ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY
* OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY,
* TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL TEXAS INSTRUMENTS
* OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
* BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
* INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES,
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
* CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
* Note:
* 40000 / 20ms = 2000 per ms = 2 counts per uS
* for servo 0.5ms 		 -> 1000
* for servo 1ms (-90deg) -> 2000
* for servo 1.5ms (0deg) -> 3000
* for servo 2ms (+90deg) -> 4000
*/

#include "servo.h"
#include "../HAL/hal_cc2530znp_target_board.h"
#include <string.h>
#include "../Common/printf.h"
#include "pin.h"
#include "../HAL/hal_cc2530znp_target_board.h"

#define PWM1	BIT4	// P4.4
#define PWM2	BIT5	// P4.5

unsigned int servo_period		= 40000;// 50Hz
unsigned int servo_period1		= 1300; //1200;
unsigned int servo_period2		= 5300;

void servoInit(void)
{
	  P4OUT &= ~BIT4;				// P4.4 = 0
	  P4DIR |= BIT4;				// P4.4 is output
	  P4SEL |= BIT4;				// select TB1 function on P4.4
	  P4OUT &= ~BIT5;				// P4.5 = 0
	  P4DIR |= BIT5;				// P4.5 is output
	  P4SEL |= BIT5;				// select TB1 function on P4.5
	  TBCCTL1 = OUTMOD_7;			// TBCCR1 reset/set
	  TBCCTL2 = OUTMOD_7;			// TBCCR1 reset/set
	  TBCTL= TBSSEL_2 + ID_1 + MC_1;// SMCLK, divide by 2, upmode
	  TBCCR0 = servo_period-1; 		// PWM Period
	  TBCCR1 = servo_period1; 		// TBCCR1
	  TBCCR2 = servo_period2; 		// TBCCR2
}

void setServo0(int param) // parameter: 0 - 180 degree
{
	  TBCCR1 = 1300 + 22 * param; 		// TBCCR1
}

void setServo1(int param) // parameter: 0 - 180 degree
{
	  TBCCR2 = 1300 + 22 * param; 		// TBCCR1
}
