#include <string.h>
#include "../Common/printf.h"
#include "voltage.h"

char *getVoltage(Voltage *voltage)
{
	if(voltage->inter.enable == false)
		takeValueVoltage(voltage);
	if(voltage->send == false)
		return "";
	char tmp[20]= "";
	strcat(tmp, ";volt:");
	strcat(tmp, p_int(voltage->value));
	return tmp;
}

char *getVoltageConfig(Voltage voltage)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:voltage;conf:{inf:");
	strcat(tmp, p_int(voltage.inter.inf));
	strcat(tmp, ";sup:");
	strcat(tmp, p_int(voltage.inter.sup));
	strcat(tmp, ";dif:");
	strcat(tmp, p_int(voltage.inter.dif));
	strcat(tmp, ";en:");
	strcat(tmp, p_bool(voltage.inter.enable));
	strcat(tmp, ";send:");
	strcat(tmp, p_bool(voltage.send));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setVoltageConfig(Voltage *voltage, char *conf)
{
	printf("Change voltage config");
	if (findAttr(conf, "inf"))
		voltage->inter.inf = atoi(findAttr(conf, "inf"));
	if (findAttr(conf, "sup"))
		voltage->inter.sup = atoi(findAttr(conf, "sup"));
	if (findAttr(conf, "dif"))
		voltage->inter.dif = atoi(findAttr(conf, "dif"));


	if (findAttr(conf, "send"))
		if (atoi(findAttr(conf, "send")) == 1)  voltage->send = true;
		else voltage->send = false;
	if (findAttr(conf, "enable"))
		if (atoi(findAttr(conf, "en")) == 1)  voltage->inter.enable = true;
		else voltage->inter.enable = false;

	return getVoltageConfig(*voltage);
}


bool interrVoltage(Voltage *voltage)
{
	static int lastvaluevoltage;
	int inf = voltage->inter.inf;
	int sup = voltage->inter.sup;
	int dif = voltage->inter.dif;

	if(voltage->inter.enable)
	{
		int value = takeValueVoltage(voltage);

		if(inf == 0 && sup == 0 && dif == 0)// sending every time : DANGEROUS because it send a lot of message
		{	 	 	 	 	 	 	 	 	// and you can't change after this. need to reboot the sensor
			if (value != lastvaluevoltage)
			{
				lastvaluevoltage = value;
				return true;
			}
			return false;
		}
		else								// we need to apply a filter
		{
			if( (inf > 0 && value < inf) || (sup > 0 && value > sup) || dif > 0)
			{
				if(dif >0)
				{
					if(abs(lastvaluevoltage-value) > dif)
					{
						lastvaluevoltage = value;
						return true;
					}
					else
						return false;
				}
				else
				{
					lastvaluevoltage = value;
					return true;
				}
			}
			return false;
		}
	}
	return false;
}

int takeValueVoltage(Voltage *voltage)
{
	voltage->value =getVcc3();
	return voltage->value;
}
