#ifndef I2C_H
#define I2C_H
#include "bool.h"
#include "pin.h"
#include "io.h"

//******************************************************************************
//  MSP430F20xx - I2C Master Transmitter and Receiver via CPU Software (GPIO)
//  Description: This code library configures the MSP430 as an I2C master device
//  capable of transmitting and receiving bytes using GPIO pins.  Specific I/O
//  pins can be selected in the corresponding header file.  By default, the same
//  pins that are used by the USI module are selected.
//
//               Master MSP430
//             -----------------
//         /|\|              XIN|-
//          --|RST          XOUT|-
//            |         SDA/Px.y|-------> [I2C SLAVE SDA] in this case p4_5  -> PIN6
//            |         SCL/Px.z|-------> [I2C SLAVE SCL] in this case p4_6  -> PIN8
//
//  Note: Internal pull-ups are NOT used for SDA & SCL [DISABLED]
//  R. Wu (TI January 2010)
//******************************************************************************

#define PxSEL P4SEL                         // Port selection
#define PxDIR P4DIR                         // Port direction
#define PxOUT P4OUT                         // Port output
#define PxIN  P4IN                          // Port input

#define SDA   BIT5                          // Controls SDA line
#define SCL   BIT6                          // Controls SCL line

#define MSP430CLOCKFREQHZ 8000000           // USER CONFIG: Existing Sys Freq
#define I2CDELAYUSEC         300            // USER CONFIG: GPIO change delay (Modified from 2500
#define GPIODELAYCYCLES ((MSP430CLOCKFREQHZ/1000000)*I2CDELAYUSEC)

#define I2CDELAY I2C_delay()                // Macro for GPIO change delay
#define SDA_1 PxDIR&=~(SDA)                 // Set to input [SDA = 1 via pullup]

#define SDA_0 PxDIR|=(SDA)                  // Set to output [SDA = 0]
#define SCL_1 PxDIR&=~(SCL)                 // Set to input [SCL = 1 via pullup]
#define SCL_0 PxDIR|=(SCL)                  // Set to output [SCL = 0]

#define NACK 0
#define ACK  1

// TMP102 Sensor Configuration
#define AddrTMP102					0x48
#define AddrTemperatureRegister		0x00
#define AddrConfigRegister			0x01

char *getI2C(Io *io);
char *getI2CConfig(Io *io);
char *setI2CConfig(Io *io, char *conf);

void I2C_init(void);
void I2C_delay(void);
void I2C_start(Io *io);
void I2C_stop(Io *io);

unsigned char I2C_txByte(Io *io, unsigned char data);
unsigned char I2C_rxByte(Io *io, char ack);
void I2C_writeBlock(Io *io, unsigned char SlaveAddress,
								unsigned int numBytes, unsigned char multi,
								void* TxData);
void I2C_readBlock(Io *io, unsigned char SlaveAddress,
                               unsigned int numBytes, void* RxData);
#endif
