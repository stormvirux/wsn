#ifndef CONFIGURE_H
#define CONFIGURE_H
#define VERSION 11
#include "light.h"
#include "lqi.h"
#include "voltage.h"
#include "temp.h"
#include "accel.h"
#include "button.h"
#include "led.h"
#include "bool.h"
#include "time.h"
#include "io.h"
#include "pin.h"
#include "spi.h"
#include "i2c.h"
#include "time.h"
#include "servo.h"

/* TO DO
 * Button that returns its value and does not change
 * Problem with LQI value when below 100 */

typedef struct Config
{
	unsigned char name[20];
	int 	channel;
	bool	send;
	Light	light;
	Lqi		lqi;
	Voltage	voltage;
	Temp	temp;
	Accel	accel;
	Led		led;
	Button	button;
	Time	time;
	Io		io;
	Servo 	servo;

} Config;

char *getPanId();
char *getName(Config config);
char *getChannel(Config config);
void changeChannel(Config *config, int channel);
void loadConfig(Config *config, int x);
void dataToJson(Config config,  unsigned char* tmp);
int  command(Config *config, char *buffer, unsigned char* tmp);
void concatenate_string(char *original, char *add);

#endif
