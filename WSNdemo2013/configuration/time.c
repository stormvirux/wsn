#include <string.h>
#include "../Common/printf.h"
#include "time.h"

void initTime(Time time)
{
	if(time.enable == true)
		initTimer(time.value, 1);	// was 1
		//printf("TIMER: %d\r\n", initTimer(time.value, 1));
}

void enableTime(Time *time, bool en)
{
	time->enable = en;
	if(en == true)
		initTimer(time->value, 1);
	else
		stopTimer();
}

bool enabledTime(Time time)
{
	return time.enable;
}

char *getTimeConfig(Time *time)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:time;conf:{sec:");
	strcat(tmp, p_int(time->value));
	strcat(tmp, ";en:");
	strcat(tmp, p_int(time->enable));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setTimeConfig(Time *time, char *conf)
{
	printf("Change Time config\r\n");
	if (findAttr(conf, "sec"))
	{
		time->value = atoi(findAttr(conf, "sec"));
		stopTimer();
		initTimer(time->value, 1);
	}
	if (findAttr(conf, "en"))
	{
		if (atoi(findAttr(conf, "en")) == 1)
			time->enable= true;
		else
			time->enable = false;
	}
	return getTimeConfig(time);
}

char *setTime(Time *time, int value)
{
	stopTimer();
	time->value = value;
	initTimer(value, 1);
	return "1";
}

int getTime(Time time)
{
	return time.value;
}

bool interrTime(Time *time)
{
	if(time->inter == true)
	{
		time->inter = false;
		return true;
	}
	return false;
}
