#include <string.h>
#include "../Common/printf.h"
#include "Button.h"
#include "../HAL/hal_cc2530znp_target_board.h"

static bool lastValueButton;

char *getButton(Button button)
{
	if (button.send == false)
		return "";
	char tmp[20]= "";
	strcat(tmp, ";button:");
	if (P1IN >= 4)
		strcat(tmp,"0");
	else
		strcat(tmp,"1");
	return tmp;
}

char *getButtonConfig(Button button)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:button;conf:{send:");
	strcat(tmp, p_bool(button.send));
	strcat(tmp, ";inter:");
	strcat(tmp, p_bool(button.inter));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setButtonConfig(Button *button, char *conf)
{
	// Take, convert and put all values in the structure
	if (findAttr(conf, "send"))
		if (atoi(findAttr(conf, "send")) == 1) button->send = true;
		else button->send = false;
	if (findAttr(conf, "inter"))
		if (atoi(findAttr(conf, "inter")) == 1) button->inter = true;
		else button->inter = false;
	// Apply all difference
	return getButtonConfig(*button);
}

bool interrButton(Button *button)
{
	if(button->inter == true)
	{
		if( button->value != lastValueButton)
		{
			lastValueButton = button->value;
			return true;
		}
		return true;
	}
	return false;
}
