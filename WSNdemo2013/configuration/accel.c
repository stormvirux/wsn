#include <string.h>
#include "../HAL/hal_vti_cma3000_D01_accelerometer.h"
#include "../HAL/hal_cc2530znp_target_board.h"
#include "../Common/printf.h"
#include "accel.h"

# define INTERX		0x01
# define INTERY		0x02
# define INTERZ		0x04
# define DIF		10 // 5 Diff between 2 Values

void initAccel(Accel *accel)
{
	halSpiInitAccelerometer();
	writeAccelerometerRegister(ACCEL_CTRL, G_RANGE_2 | I2C_DIS | MODE_100 | INT_DIS);
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);
	readAccelerometerRegister(ACCEL_INT_STATUS);
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);
}

char *getAccel(Accel *accel)
{
	takeValueAccel(accel);
	if( accel->sendx == false && accel->sendy == false && accel->sendz == false)
		return "";
	char tmp[50]= ";accel:[";
	int coma = 0;
	if( accel->sendx == true)
	{
		strcat(tmp, "X:");
		strcat(tmp, p_int(accel->x));
		coma =1;
	}
	if( accel->sendy == true)
	{
		if(coma) strcat(tmp, ",");
		strcat(tmp, "Y:");
		strcat(tmp, p_int(accel->y));
		coma =1;
	}
	if( accel->sendz == true)
	{
		if(coma) strcat(tmp, ",");
		strcat(tmp, "Z:");
		strcat(tmp, p_int(accel->z));
	}
	strcat(tmp, "]");
	return tmp;
}

char *getAccelConfig(Accel accel)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:accel;conf:{sendx:");
	strcat(tmp, p_int(accel.sendx));
	strcat(tmp, ";sendy:");
	strcat(tmp, p_int(accel.sendy));
	strcat(tmp, ";sendz:");
	strcat(tmp, p_int(accel.sendz));
	strcat(tmp, ";mode:");
	strcat(tmp, p_int(accel.mode));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setAccelConfig(Accel *accel, char *conf)
{
	// Take, convert, and put all values in the structure
	if (findAttr(conf, "sendx"))
		accel->sendx = atoi(findAttr(conf, "sendx"));
	if (findAttr(conf, "sendy"))
		accel->sendy = atoi(findAttr(conf, "sendy"));
	if (findAttr(conf, "sendz"))
		accel->sendz = atoi(findAttr(conf, "sendz"));
	if (findAttr(conf, "mode"))
		accel->mode = atoi(findAttr(conf, "mode"));
	return getAccelConfig(*accel);
}

bool interrAccel(Accel *accel)
{
	static unsigned char lvaluex, lvaluey, lvaluez;
	if(accel->mode  != 0x00)
	{
		takeValueAccel(accel);
		if( (accel->mode | INTERX)  && abs(lvaluex- accel->x) >= DIF) // if inter x and diff too big
		{
			lvaluex = accel->x;
			lvaluey = accel->y;
			lvaluez = accel->z;
			return true;
		}
		if( (accel->mode | INTERY)  && abs(lvaluey -accel->y) >= DIF) // if inter x and diff too big
		{
			lvaluex = accel->x;
			lvaluey = accel->y;
			lvaluez = accel->z;
			return true;
		}
		if( (accel->mode | INTERZ)  && abs(lvaluez- accel->z) >= DIF) // if inter x and diff too big
		{
			lvaluex = accel->x;
			lvaluey = accel->y;
			lvaluez = accel->z;
			return true;
		}
	}
	return false;
}

int takeValueAccel(Accel *accel)
{
	accel->x = readAccelerometerRegister(ACCEL_DOUTX);
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);
	accel->y = readAccelerometerRegister(ACCEL_DOUTY);
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);
	accel->z = readAccelerometerRegister(ACCEL_DOUTZ);
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);
	delayUs(ACCELEROMETER_DELAY_BETWEEN_OPERATIONS_US);
	// was delayMs(300);
	//delayMs(1); // ??????????
	return 1;
}
