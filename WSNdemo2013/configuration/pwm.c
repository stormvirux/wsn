/**
 * \file 	pwm.c
 * \brief 	low level functions for controlling PWM with MSP430F2740
 * \author 	Jan Szymanski
 * \date	September 2012
 *
* YOU ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY
* OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY,
* TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL TEXAS INSTRUMENTS
* OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION,
* BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
* INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES,
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY
* CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*/

#include "pwm.h"
#include "../HAL/hal_cc2530znp_target_board.h"
#include <string.h>
#include "../Common/printf.h"
#include "pin.h"
#include "../HAL/hal_cc2530znp_target_board.h"

unsigned int pwm_period		= 100;	// 20 kHz
unsigned int pwm_duty1		= 75;	// duty cycle
unsigned int pwm_duty2		= 90;	// duty cycle

void pwmInit(void)
{
	  P4OUT &= ~BIT4;				// P4.4 = 0
	  P4DIR |= BIT4;				// P4.4 is output
	  P4SEL |= BIT4;				// select TB1 function on P4.4
	  P4OUT &= ~BIT5;				// P4.5 = 0
	  P4DIR |= BIT5;				// P4.5 is output
	  P4SEL |= BIT5;				// select TB1 function on P4.5
	  TBCCTL1 = OUTMOD_7;			// TBCCR1 reset/set
	  TBCCTL2 = OUTMOD_7;			// TBCCR1 reset/set
	  TBCTL= TBSSEL_2 + ID_1 + MC_1;// SMCLK, divide by 2, upmode
	  TBCCR0 = pwm_period-1; 		// PWM Period
	  TBCCR1 = pwm_duty1; 			// TBCCR1 PWM Duty Cycle
	  TBCCR2 = pwm_duty2; 			// TBCCR2 PWM Duty Cycle
}
