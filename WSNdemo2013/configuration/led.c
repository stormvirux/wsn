#include <string.h>
#include "../Common/printf.h"
#include "led.h"

char *getLed(Led led)
{
	char tmp[20]= "";
	if(led.send1 == true)
	{
		strcat(tmp, ";led1:");
		strcat(tmp, p_bool(led.led1));
	}
	if(led.send2 == true)
	{
		strcat(tmp, ";led2:");
		strcat(tmp, p_bool(led.led2));
	}
	return tmp;
}

char *getLedConfig(Led led)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:led;conf:{send1:");
	strcat(tmp, p_int(led.send1));
	strcat(tmp, ";send2:");
	strcat(tmp, p_int(led.send2));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setLedConfig(Led *led, char *conf)
{
	// take, convert, and put all values in the structure
	if (findAttr(conf+1, "led1"))
		led->led1 = atoi(findAttr(conf+1, "led1"));
	if (findAttr(conf+1, "led2"))
		led->led2 = atoi(findAttr(conf+1, "led2"));
	if (findAttr(conf+1, "send1"))
		led->send1 = atoi(findAttr(conf+1, "send1"));
	if (findAttr(conf+1, "send2"))
		led->send2 = atoi(findAttr(conf+1, "send2"));
	clearLeds();
	if (led->led1)
		setLed(0);
	if (led->led2)
		setLed(1);
	return getLedConfig(*led);
}
