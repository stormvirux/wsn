/**
 * \file	pwm.h
 * \brief	public methods for pwm.c
 * \author 	Jan Szymanski
 * \date	September 2012
 */

#ifndef PWM_H
#define PWM_H

#include "bool.h"
#include "pin.h"
#include "io.h"

void pwmInit(void);

#endif
