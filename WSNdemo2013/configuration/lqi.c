#include <string.h>
#include "../Common/printf.h"
#include "lqi.h"

char *getLqi(Lqi *lqi)
{
	if(lqi->send == false)
		return "";
	char tmp[20]= "";
	strcat(tmp, ";lqi:");
	strcat(tmp, "XXX");
	return tmp;
}

char *getLqiConfig(Lqi lqi)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:lqi;conf:{send:");
	strcat(tmp, p_bool(lqi.send));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setLqiConfig(Lqi *lqi, char *conf)
{
	printf("Change LQI Config\r\n");
	if (findAttr(conf, "send"))
		if (atoi(findAttr(conf, "send")) == 1)  lqi->send = true;
		else lqi->send = false;
	return getLqiConfig(*lqi);
}
