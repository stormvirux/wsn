#ifndef SPI_H
#define SPI_H
#include "bool.h"
#include "pin.h"
#include "io.h"

//******************************************************************************
//  MSP430F20xx - SPI Master Transmitter and Receiver via CPU Software (GPIO) and SPI Connection
//				DEBUG PORT
//				 _______
//				|o	   o|
//				|o	   o|
//				|o	   o|--> CLK p0_5
//				|o	   o|--> SI  p0_3
//				|o	   o|--> SO  p0_2
//				|_______|
//
//******************************************************************************
#define ONEREG 		0x01
#define MPL 		0x10

char *getSPI(Io *io);
char *getSPIConfig(Io *io);
char *setSPIConfig(Io *io, char *value);

#endif
