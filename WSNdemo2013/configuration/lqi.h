#ifndef LQI_H
#define LQI_H
#include "bool.h"

typedef struct Lqi
{
	bool send;
} Lqi;

char *getLqi(Lqi *lqi);
char *getLqiConfig(Lqi lqi);
char *setLqiConfig(Lqi *lqi, char *conf);

#endif
