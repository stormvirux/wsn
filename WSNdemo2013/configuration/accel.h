#ifndef ACCEL_H
#define ACCEL_H
#include "bool.h"

typedef struct Accel
{
	int x, y, z;
	bool sendx, sendy, sendz;
	unsigned char mode;
} Accel;

void initAccel();
char *getAccel(Accel *accel);
char *getAccelConfig(Accel accel);
char *setAccelConfig(Accel *accel, char *conf);
bool interrAccel(Accel *accel);

#endif
