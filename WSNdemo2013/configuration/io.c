#include <string.h>
#include "../Common/printf.h"
#include "../HAL/hal_cc2530znp_target_board.h"
#include "io.h"
#include "configure.h"

void initIo(Io *io)
{
	int i;
	for (i=0; i < 6 ; i++)
	{
		initPin(&(io->pin[i]));
	}
}

char *getIoConfig(Io *io)
{
	unsigned char t = 0;
	int r = 0;
	char tmp[80]= "";
	strcat(tmp, ";dev:io;conf:{send:");
	t = io->pin[0].send + io->pin[2].send*2 + io->pin[5].send*4 +  io->pin[0].send*8 + io->pin[2].send*16 + io->pin[5].send*32;
	strcat(tmp, hexToChar(t));
	strcat(tmp, ";mode:");
	r = io->pin[0].mode + io->pin[2].mode*4 + io->pin[5].mode*16 +  io->pin[0].mode*64 + io->pin[2].mode*256 + io->pin[5].mode*1024;
	strcat(tmp, hexToChar(r));
	strcat(tmp, ";dir:");
	t = io->pin[0].direction + io->pin[2].direction*2 + io->pin[5].direction*4 + io->pin[0].direction*8 + io->pin[2].direction*16 + io->pin[5].direction*32;
	strcat(tmp, p_int(t));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setIoConfig(Io *io,char *value)
{
	char pin[3];
	if ( findAttr(value, "pin") != NULL) // configure one pin
	{
		strcpy(pin, findAttr(value, "pin"));
		switch(pin[0])
		{
			case '3': return setConfigPin(&(io->pin[0]), value);
			case '4': return setConfigPin(&(io->pin[1]), value);
			case '5': return setConfigPin(&(io->pin[2]), value);
			case '6': return setConfigPin(&(io->pin[3]), value);
			case '7': return setConfigPin(&(io->pin[4]), value);
			case '8': return setConfigPin(&(io->pin[5]), value);
		}
	}
	return NULL;
}

char *getIo(Io *io)
{
	char tmp[80] = "";
	// PIR SENSOR
	strcpy(tmp, ";X:");
	if ((P2IN & (&(io->pin[2]))->bit) == 0) // Hanse PIR is triggered when LOW
	{
		strcat(tmp, "1");
	}
	else
	{
		strcat(tmp, "0");
	}
	strcat(tmp, ";Y:");
	if ((P2IN & (&(io->pin[4]))->bit) == 0) // Futurlec PIR is triggered when HIGH
	{
		strcat(tmp, "0");
	}
	else
	{
		strcat(tmp, "1");
	}
	// FIRE ALARM
	strcat(tmp, ";Z:");
	if ((&(io->pin[0]))->mode == DIGITAL) // If PWM Wave could be captured, can be rising/falling edge
	{
		strcat(tmp, p_int((&(io->pin[0]))->bit));
	}
	else if((&(io->pin[0]))->mode == ANALOG)
	{
		P2SEL &= ~((&(io->pin[0]))->bit); // P2.2 0 GPIO
		P2DIR &= ~((&(io->pin[0]))->bit); // P2.2 0 input
		ADC10CTL0 = 0x0000;				  // Clear ADC10 Registers
		ADC10CTL1 = 0x0000;
		ADC10CTL0 = ADC10SHT_3 + ADC10ON; // Sample at 64 x ADC10CLK and enable ADC10
		ADC10CTL1 = INCH_2;				  // Select Channel 2 for P2.2
		ADC10AE1 |= (&(io->pin[0]))->bit; // P2.2 ADC option select
		ADC10CTL0 |= ENC + ADC10SC;       // Sampling and conversion start
		while (!(ADC10CTL0 & ADC10IFG));  // Wait until conversion is done
		strcat(tmp, p_int(ADC10MEM));	  // Get Data from ADC10MEM and
	}
	return tmp;
}
