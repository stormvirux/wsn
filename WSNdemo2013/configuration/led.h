#ifndef LED_H
#define LED_H
#include "bool.h"

typedef struct Led
{
	bool led1, led2;
	bool send1, send2;
}Led;

char *getLed(Led led);
char *getLedConfig(Led led);
char *setLedConfig(Led *led, char *conf);

#endif
