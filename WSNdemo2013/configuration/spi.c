#include "../HAL/hal_cc2530znp_target_board.h"
#include "../Common/printf.h"
#include <string.h>
#include "spi.h"
#include "pin.h"
#include "io.h"

char *getSPIConfig(Io *io)
{
	char tmp[80]= "";
	strcat(tmp, ";dev:spi;conf:{mode:");
	strcat(tmp, p_int(io->spi));
	strcat(tmp, ";pin:");
	strcat(tmp, p_int(io->spiNumPin+3));
	strcat(tmp, ";reg:");
	strcat(tmp, hexToChar(io->spiReg));
	strcat(tmp, "}}\n");
	return tmp;
}

char *setSPIConfig(Io *io, char *value)
{
	// SPI is already launched
	if ( findAttr(value, "mode") != NULL)
	{
		io->spi = atoi(findAttr(value, "mode"));
	}
	if ( findAttr(value, "pin") != NULL)
	{
		io->spiNumPin  = charToHex((findAttr(value, "pin")))-3;
		// configure the pin
		io->pin[io->spiNumPin].mode = DIGITAL;
		io->pin[io->spiNumPin].direction = OUTPUT;
		io->pin[io->spiNumPin].value = 1;
		configPin(&(io->pin[io->spiNumPin]));
	}
	if ( findAttr(value, "reg") != NULL)
	{
		io->spiReg  = charToHex((findAttr(value, "reg")));
	}
	//printf("Configuration of SPI\r\n");
	return getSPIConfig(io);
}

char *getSPI(Io *io)
{
	unsigned int result = 0, Tadc, Padc;
	int a0, b1, b2, c12, p;
	char tmp[20] = "";
	if (io->spi == NONE)	 // CASE WHEN SPI IS DISABLED
		return tmp;
	else if((io->spi & MPL)) // CASE WHEN SPI IS ENABLED for the MPL
	{
		io->pin[0].value = 0;
		setValuePin(io->pin[0], 0);
		result = ACCELEROMETER_RX_BUF; // Read RX buffer just to clear interrupt flag
		//launch conversion
		spiWriteAccelerometer(0x24);
		result = spiWriteAccelerometer(0x00);
		setValuePin(io->pin[io->spiNumPin], 1);
		delayMs(1);
		setValuePin(io->pin[io->spiNumPin], 0);
		// Read Temp TADC
		result= 0;
		spiWriteAccelerometer(0x84);
		result = spiWriteAccelerometer(0x00);   // Read the selected register
		result = result << 8;
		spiWriteAccelerometer(0x86);    		// Read the selected register
		result |= spiWriteAccelerometer(0x00);  // Read the selected register
		result = result >> 6;
		Tadc = result;
		//printf("Monitored Value of SPI Temperature: %d\r\n", Tadc);
		// Read Pressure PADC
		result= 0;
		spiWriteAccelerometer(0x80);
		result = spiWriteAccelerometer(0x00);   // Read the selected register
		result = result << 8;
		spiWriteAccelerometer(0x82);    		// Read the selected register
		result |= spiWriteAccelerometer(0x00);  // Read the selected register
		result = result >> 6;
		Padc = result;
		//printf("Monitored Value of SPI Pressure: %d\r\n", Padc);
		// Read A0
		a0= 0;
		spiWriteAccelerometer(0x88);
		a0 = spiWriteAccelerometer(0x00);    	// Read the selected register
		a0 = a0 << 8;
		spiWriteAccelerometer(0x8A);    		// Read the selected register
		a0 |= spiWriteAccelerometer(0x00);		// Read the selected register
		//printf("Monitored Value of a0: %d\r\n", a0);
		// READ D1
		b1= 0;
		spiWriteAccelerometer(0x8C);
		b1 = spiWriteAccelerometer(0x00);   	// Read the selected register
		b1 = b1 << 8;
		spiWriteAccelerometer(0x8E);    		// Read the selected register
		b1 |= spiWriteAccelerometer(0x00);    	// Read the selected register
		//printf("Monitored Value of b1: %d\r\n", b1);
		// READ B2
		b2= 0;
		spiWriteAccelerometer(0x90);
		b2 = spiWriteAccelerometer(0x00);    // Read the selected register
		b2 = b2 << 8;
		spiWriteAccelerometer(0x92);    // Read the selected register
		b2 |= spiWriteAccelerometer(0x00);    // Read the selected register
		//printf("Monitored Value of b2: %d\r\n", b2);
		// READ C12
		c12= 0;
		spiWriteAccelerometer(0x94);
		c12 = spiWriteAccelerometer(0x00);    // Read the selected register
		c12 = c12 << 8;
		spiWriteAccelerometer(0x96);    // Read the selected register
		c12 |= spiWriteAccelerometer(0x00);    // Read the selected register
		c12 = c12 >>2;
		//printf("Monitored Value of c12: %d\r\n", c12);
		// Calculate KPA Levels
		p = ((a0/8.000 + (b2/16384.000) * Tadc+(b1/8192.000 + ((c12/4.0)/4194304.0)*(Tadc*1.00))*Padc)*(115-50)/1023.0)+50;
		//printf("RESULT = %d KPA\r\n", p);
		io->pin[io->spiNumPin].value = 1;
		setValuePin(io->pin[io->spiNumPin], 1);
		strcpy(tmp, ";spib:");
		strcat(tmp, p_int(p));
	}
	// IF WE WANT TO READ OR WRITE A VALUE
	else if ( io->spi & ONEREG)
	{
		io->pin[io->spiNumPin].value = 0;
		setValuePin(io->pin[io->spiNumPin], 0);
		spiWriteAccelerometer(io->spiReg);
		b1 = spiWriteAccelerometer(0x00); // Read the selected register
		io->pin[io->spiNumPin].value = 1;
		setValuePin(io->pin[io->spiNumPin], 1);
		strcpy(tmp, ";spi:");
		strcat(tmp, p_int(b1));
	}
	return tmp;
}
