var express = require("express");
var app = express();
var port = 3700;

app.set('views', __dirname + '/template');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
app.get("/", function(req, res) {
	res.render("page");
});

// app.get("/", function(req, res) {
// 	res.send("It works!");
// });

app.use(express.static(__dirname + '/public'));

// app.listen(port);

// Use and integrate socket.io instead. This passes the ExpressJS server to Socket.io
var io = require('socket.io').listen(app.listen(port));

// Code that receives a message from client and send it to others.
io.sockets.on('connection', function (socket) {
	socket.emit('message', { message: 'Start session. Test. '});
	socket.on('send', function (data) {
		io.sockets.emit('message', data);
	});
});


console.log("Listening on port " + port);