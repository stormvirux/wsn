window.onload = function() {
	var messages = [];
	var socket = io.connect('http://localhost:3700');
	var field = document.getElementById("field");
	var sendButton = document.getElementById("send");
	var content = document.getElementById("content");

	socket.on('message', function (data) {
		if (data.message) {
			messages.push(data.message);
			var html = '';
			for (var i = 0; i < messages.length; i++) {
				html += messages[i] + '<br />';
			}
			content.innerHTML = html;
		} else {
			console.log("There is a problem: ", data);
		}
	});

	var coordinator = io.connect('http://localhost:3000')

	coordinator.on('coord_received', function (data) {
		var text = data;
		socket.emit('send', {message: text})
	});


	sendButton.onclick = function () {
		var text = field.value;
		socket.emit('send', {message: text})
	};
}